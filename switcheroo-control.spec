Name:           switcheroo-control
Version:        1.1
Release:        8
Summary:        D-Bus service to check the availability of dual-GPU
License:        GPLv3
URL:            https://gitlab.freedesktop.org/hadess/switcheroo-control/
Source0:        https://github.com/hadess/switcheroo-control/releases/download/%{version}/switcheroo-control-%{version}.tar.xz

BuildRequires:  pkgconfig(gio-2.0) systemd gcc
%{?systemd_requires}

%description
D-Bus service to check the availability of dual-GPU.

%prep
%autosetup -n switcheroo-control-%{version} -p1

%build
%configure
%make_build

%install
%make_install

%post
%systemd_post switcheroo-control.service

%preun
%systemd_preun switcheroo-control.service

%postun
%systemd_postun_with_restart switcheroo-control.service

%files
%doc COPYING NEWS README.md
%{_sysconfdir}/dbus-1/system.d/net.hadess.SwitcherooControl.conf
%{_unitdir}/switcheroo-control.service
%{_sbindir}/switcheroo-control

%changelog
* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 1.1-8
- Completing build dependencies to fix gcc compiler missing error

* Sat Nov 30 2019 lingsheng <lingsheng@huawei.com> - 1.1-7
- Package init
